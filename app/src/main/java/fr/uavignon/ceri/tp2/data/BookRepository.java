package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;
public class BookRepository {
    private BookDao mBookDao;
    private LiveData<List<Book>> mAllBooks;
    private MutableLiveData<Book> mBook = new MutableLiveData<>();;
    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        mBookDao = db.BookDao();

        mAllBooks =  mBookDao.getAllBooks();

    }

    public LiveData<List<Book>> getAllBooks() {
        return mAllBooks;
    }

    public void insertBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            mBookDao.insertBook(book);
        });
    }

    public void deleteProduct(Book book) {
        databaseWriteExecutor.execute(() -> {
            mBookDao.deleteBook(book);
        });
    }
    public void getBook(long id) {

        Future<Book> fproducts =  BookRoomDatabase.databaseWriteExecutor.submit(() -> {
             return mBookDao.getBook(id);
             });
        try {
            mBook.setValue(fproducts.get());
             } catch (ExecutionException e) {
             e.printStackTrace();
             } catch (InterruptedException e) {
             e.printStackTrace();
             }

    }
    public MutableLiveData<Book> findone() {



        return mBook;
    }
    public void updateBook(Book book) {


        databaseWriteExecutor.execute(() -> {
            mBookDao.updateBook(book);
        });
    }
}
