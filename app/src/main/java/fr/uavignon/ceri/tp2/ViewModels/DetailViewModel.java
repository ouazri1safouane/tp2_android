package fr.uavignon.ceri.tp2.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> one;
    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        one = repository.findone();
    }

    public MutableLiveData<Book> findone() {

        return one;
    }
    public void getBook(long i) {
        repository.getBook(i);
    }
    public void upadteorinsertBook(Book b,long u) {
        if(u == -1){ repository.insertBook(b);}
        else{repository.updateBook(b);}
    }
}
