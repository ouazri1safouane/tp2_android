package fr.uavignon.ceri.tp2.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BookDao {
    @Update
   void updateBook(Book book);
    @Insert
   void  insertBook(Book book);
    @Query("SELECT * FROM book WHERE id = :id")
     Book getBook(long id);
    @Delete
    void deleteBook(Book book);
    @Query("SELECT * FROM Book")
    LiveData<List<Book>> getAllBooks();
    @Query("Delete  FROM Book")
    void deleteAllBooks();
}
